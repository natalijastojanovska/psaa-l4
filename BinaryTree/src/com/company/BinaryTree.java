package com.company;

class Node{
    int data;
    Node leftChild;
    Node rightChild;

    public Node(int data) {
        this.data = data;
        leftChild = rightChild = null;
    }

}

public class BinaryTree {

    Node root;

    BinaryTree(int data) {
        root = new Node(data);
    }

    BinaryTree(){
        root = null;
    }

    //1. zadacha
    public int countNodes(Node root){
        if(root==null){
            return 0;
        }
        return 1 + countNodes(root.leftChild) + countNodes(root.rightChild);
    }

    //2.zadacha
    public int height(Node root){
        if(root==null){
            return 0;
        }
        return 1 + Math.max(height(root.leftChild), height(root.rightChild));
    }

    //3. zadacha
    public Node mirrorTree(Node node){
        if(node == null){
            return node;
        }

        Node left = mirrorTree(node.leftChild);
        Node right = mirrorTree(node.rightChild);

        node.leftChild = right;
        node.rightChild = left;

        return node;
    }

    public void print(Node root, int level) {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.data + " ");
        else if (level > 1) {
            print(root.leftChild, level - 1);
            print(root.rightChild, level - 1);
        }
    }
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();

        tree.root = new Node(1);

        tree.root.leftChild = new Node(2);
        tree.root.rightChild = new Node(3);

        tree.root.leftChild.leftChild = new Node(4);
        tree.root.leftChild.rightChild = new Node(5);

        tree.root.rightChild.leftChild = new Node(6);
        tree.root.rightChild.rightChild = new Node(7);

        System.out.println(tree.countNodes(tree.root)); //1. zadacha

        System.out.println("-----");

        System.out.println(tree.height(tree.root)); //2. zadacha

        System.out.println("-----");

        int n = tree.height(tree.root);
        while(n != 0) {   //3. zadacha
            tree.print(tree.root, n);
            n--;
            System.out.println();
        }

        tree.mirrorTree(tree.root);

        System.out.println("-----");

        n = tree.height(tree.root);
        while(n != 0) {   //3. zadacha
            tree.print(tree.root, n);
            n--;
            System.out.println();
        }
    }
}
